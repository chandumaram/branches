export class User {
    FirstName:string;
    LastName:string;
    UserName:string;
    Gender:string;
    DOB:Date;
    Village:string;
    State:string;
    Country:string;
    MaritalStatus:string;
    Email:string;
    FbId:string;
    TwitterId :string;
}
