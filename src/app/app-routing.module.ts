import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalComponent } from './personal/personal.component';
import { NavComponent } from './nav/nav.component';
import { QualificationComponent } from './qualification/qualification.component';
import { ContactComponent } from './contact/contact.component';


const routes: Routes = [

  {path:' ',component:NavComponent},
  {path:'personal',component:PersonalComponent},
  {path:'qualification',component:QualificationComponent},
  {path:'contact',component:ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
