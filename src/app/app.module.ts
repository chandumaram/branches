import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule,MatCardModule,MatGridListModule,MatFormFieldModule,MatInputModule,MatSelectModule} from '@angular/material';
import { UserService } from './user.service';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { PersonalComponent } from './personal/personal.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ContactComponent } from './contact/contact.component';
import { QualificationComponent } from './qualification/qualification.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    PersonalComponent,
    ContactComponent,
    QualificationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
