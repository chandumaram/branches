import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  Details: Observable<User[]>;
  isReadonly = true;

  constructor(private service: UserService) { }

  ngOnInit() {
    
    this.AllDetails();
  }

  AllDetails() {
    this.Details = this.service.getUser();
  }
  editDetails() {
    this.isReadonly = !this.isReadonly;
  }

}
