import { Component, OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  PersonalDetails: Observable<User[]>;
  isReadonly = true;
  isUserName = null;
  userForm: any;

  constructor(private service: UserService) { }

  ngOnInit() {
    
    this.AllDetails();
  }

  AllDetails() {
    this.PersonalDetails = this.service.getUser();
  }
  editDetails() {
    this.isReadonly = !this.isReadonly;
  }
    /*
  onFormSubmit() {

    const user = this.userForm.value;
    this.UpdateUser(user);
  }
  UpdateUser(user: User) {
    user.UserName = this.isUserName;
    this.service.updateUser(user).subscribe(() => {
      this.AllDetails();
    });
    this.isReadonly = true;
  }
*/
 SubmitDetails(user:User){  
  
   user.UserName=this.isUserName;
     this.service.updateUser(user).subscribe(() => {  
       this.AllDetails(); 
       
     });
     
     this.isReadonly=true;
 }

}
